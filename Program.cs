﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            string ano = System.DateTime.Today.Year.ToString(); // System.DateTime.Today.Year.ToString("dd/MM/yyyy");
            string mes = System.DateTime.Today.Month.ToString();  // System.DateTime.Today.Month.ToString("dd/MM/yyyy");//
            string dia = System.DateTime.Today.Day.ToString();    // System.DateTime.Today.ToString("dd/MM/yyyy");

            string strCmdText;
            strCmdText = $"/C runemacs --eval \"(progn (find-file (format-time-string " + @"\" + $"\"/Tareo/{ano}-{sumarCeroFecha(mes)}-{sumarCeroFecha(dia)}.csv" + @"\" + $"\")) (set-background-color"  + @"\" + "\"NavyBlue" + @"\" + "\") (toggle-frame-maximized) (end-of-buffer) (newline) (timestamp))\"";
            //Console.WriteLine(strCmdText);
            //Console.ReadLine();
            System.Diagnostics.Process.Start("CMD.exe", strCmdText);
        }

        public static string sumarCeroFecha(string MesDia)
        {
            if (MesDia.Length == 1)
            {
                return "0" + MesDia;
            }
            else
            {
                return MesDia;
            }

        }
    }
}
